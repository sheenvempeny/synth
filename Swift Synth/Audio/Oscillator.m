//
//  Oscillator.m
//  Swift Synth
//
//  Created by Sheen on 28.01.20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Oscillator.h"
#include <math.h>


@interface Oscillator()

+ (float)randomNumberBetween:(float)min maxNumber:(float)max;

@end

static float _amplitude = 1;
static float _frequency = 440;

static Signal _sine =  ^float(float time) {
	return Oscillator.amplitude * sin(2.0 * M_PI * Oscillator.frequency * time);
};

static Signal _triangle = ^float(float time) {
	double period = 1.0 / (double)Oscillator.frequency;
	double currentTime = fmod((double)time, period);
	 
	double value = currentTime / period;
	 
	double result = 0.0;
	 
	 if (value < 0.25) {
		 result = value * 4;
	 }
	 else if (value < 0.75) {
		 result = 2.0 - (value * 4.0);
	 }
	 else {
		 result = value * 4 - 4.0;
	 }
	 
	return Oscillator.amplitude * (float)result;
};

static Signal _whiteNoise = ^float(float time) {
	float period = 1.0 / Oscillator.frequency;
	float currentTime = fmod((double)time, (double)period);

	return Oscillator.amplitude * (((float)currentTime / period) * 2 - 1.0);
};

static Signal _square = ^float(float time) {
	double period = 1.0 / (double)Oscillator.frequency;
	double currentTime = fmod((double)time, period);

	return ((currentTime / period) < 0.5) ? Oscillator.amplitude : -1.0 * Oscillator.amplitude;

};

static Signal _sawtooth = ^float(float time) {
	float random = [Oscillator randomNumberBetween:-1 maxNumber:1];
	return Oscillator.amplitude * random;
};

@implementation Oscillator

@dynamic amplitude;
@dynamic frequency;

@dynamic sine;
@dynamic triangle;
@dynamic whiteNoise;
@dynamic square;
@dynamic sawtooth;

+ (float)randomNumberBetween:(float)min maxNumber:(float)max
{
    return min + arc4random_uniform(max - min + 1);
}

+ (float)amplitude {
	return _amplitude;
}

+ (float)frequency {
	return _frequency;
}

+ (Signal)sine {
	return _sine;
}

+ (Signal)triangle {
	return _triangle;
}

+ (Signal)whiteNoise {
	return _whiteNoise;
}

+ (Signal)square {
	return _square;
}

+ (Signal)sawtooth {
	return _sawtooth;
}

@end
