//
//  Oscillator.h
//  Swift Synth
//
//  Created by Sheen on 28.01.20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef float(^Signal)(float);

typedef enum Waveform : int {
	sine,
	triangle,
	sawtooth,
	square,
	whiteNoise
} Waveform;


@interface Oscillator: NSObject

@property (class, nonatomic, assign) float amplitude;
@property (class, nonatomic, assign) float frequency;
@property (class, nonatomic) Signal sine;
@property (class, nonatomic) Signal triangle;
@property (class, nonatomic) Signal sawtooth;
@property (class, nonatomic) Signal square;
@property (class, nonatomic) Signal whiteNoise;

@end


NS_ASSUME_NONNULL_END
