//
//  Synth.m
//  Swift Synth
//
//  Created by Sheen on 02.02.20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Synth.h"
#import <AVFoundation/AVFoundation.h>

@interface Synth()

@property (nonatomic, copy) Signal signal;
@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic, assign) float time;
@property (nonatomic, assign) double sampleRate;
@property (nonatomic, assign) float deltaTime;
@property (nonatomic, strong) AVAudioSourceNode *sourceNode;

- (instancetype)initWith:(Signal)signal;

@end

@implementation Synth

+ (Synth*)shared {
    static Synth *sharedSynth = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSynth = [[self alloc] initWith:Oscillator.sine];
    });
    return sharedSynth;
}

- (AVAudioSourceNode *)sourceNode {
	if (_sourceNode == nil) {
		_sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL * _Nonnull isSilence, const AudioTimeStamp * _Nonnull timestamp, AVAudioFrameCount frameCount, AudioBufferList * _Nonnull audioBufferList) {
			__weak typeof(self) weakSelf = self;
			for (AVAudioFrameCount frame = 0; frame < frameCount; frame++) {
				float sampleVal = weakSelf.signal(weakSelf.time);
				weakSelf.time += weakSelf.deltaTime;

				 for (int index = 0; index < audioBufferList->mNumberBuffers; index++) {
					 float *data = audioBufferList->mBuffers[index].mData;
					 data[frame] = sampleVal;
				}
			}
			return noErr;
		}];
	}

	return _sourceNode;
}

- (instancetype)initWith:(Signal)signal
{
	self = [super init];
	if (self) {
		self.signal = signal;
		[self setUp];
	}
	return self;
}

- (void) setUp {
	self.audioEngine = [AVAudioEngine new];

	AVAudioMixerNode *mainMixer = self.audioEngine.mainMixerNode;
	AVAudioOutputNode *outputNode = self.audioEngine.outputNode;
	AVAudioFormat *format = [outputNode inputFormatForBus:0];
		   
	self.sampleRate = format.sampleRate;
	self.deltaTime = 1 / (float)self.sampleRate;
	AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat sampleRate:self.sampleRate channels:1 interleaved:format.isInterleaved];
	[self.audioEngine attachNode:self.sourceNode];
	[self.audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
	[self.audioEngine connect:mainMixer to:outputNode format:nil];
	mainMixer.outputVolume = 0;
	NSError *error = nil;
	[self.audioEngine startAndReturnError:&error];
	if (error != nil) {
		NSLog(@"Could not start audioEngine: %@", error.localizedDescription);
	}
}

- (float)volume {
	return self.audioEngine.mainMixerNode.outputVolume;
}

- (void)setVolume:(float)volume {
	self.audioEngine.mainMixerNode.outputVolume = volume;
}

- (void)setWaveformTo:(Signal)signal {
	self.signal = signal;
}

- (void) updateUIForAudioPlaying: (BOOL)isPlaying {
	self.updateHader(isPlaying);
}

@end
