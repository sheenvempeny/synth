//
//  Synth.h
//  Swift Synth
//
//  Created by Sheen on 02.02.20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Oscillator.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^UIUpdateHander)(BOOL);

@interface Synth : NSObject

@property (nonatomic, assign) float volume;

+ (Synth*)shared;

@property (nonatomic, copy) UIUpdateHander updateHader;
- (void)setWaveformTo:(Signal)signal;

@end

NS_ASSUME_NONNULL_END
